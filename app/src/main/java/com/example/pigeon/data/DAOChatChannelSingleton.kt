package com.example.pigeon.data

import com.example.pigeon.model.ChatChannel

object DAOChatChannelSingleton {
    private var serial: Long = 1
    private val channels = ArrayList<ChatChannel>()

    fun addChannel() {
        val c = ChatChannel("Canal $serial ")
        this.channels.add(0, c)

        c.addUser()
        c.id = serial++
    }

    fun getChannels(): ArrayList<ChatChannel> {
        return this.channels
    }

    fun getChannelById(id: Long): ChatChannel? {
        for(c in this.channels) {
            if(c.id == id)
                return c
        }
        return null
    }

    fun getChannelPositionById(id: Long): Int {
        return this.channels.indexOf(ChatChannel(id))
    }
}