package com.example.pigeon.model

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class ChatMessage (
    val user: String,
    val message: String,
    var date: LocalDateTime,

    var id: Long = 0
){
    constructor(id: Long): this("", "", LocalDateTime.now()) {
        this.id = id
    }
    fun formatDate(): String{
        return this.date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ChatMessage

        if (user != other.user) return false
        if (message != other.message) return false
        if (date != other.date) return false
        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        var result = user.hashCode()
        result = 31 * result + message.hashCode()
        result = 31 * result + date.hashCode()
        result = 31 * result + id.hashCode()
        return result
    }

    override fun toString(): String {
        return "ChatMessage(user='$user', message='$message', date=$date, id=$id)"
    }


}

