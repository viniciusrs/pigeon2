package com.example.pigeon.model

class ChatChannel (
    val channelName: String,
    var id: Long = 0
)
{
    constructor(id: Long): this("") {
        this.id = id
    }

    private val messages = ArrayList<ChatMessage>()
    private val users = ArrayList<String>()

    fun addMessage(mensagem: ChatMessage){
        this.messages.add(mensagem)
    }

    fun addUser(){
        this.users.add("Fulano")
        this.users.add("Ciclano")
        this.users.add("Beltrano")
        this.users.add("Vinicius")
    }

    fun getMessages() : ArrayList<ChatMessage>{
        return this.messages
    }

    fun getLastMessage() : String {
        return if (this.messages.isEmpty()) {
            ""
        }else{
            val lastMessage = this.messages.last()
            val userName = lastMessage.user
            return userName + ": " + lastMessage.message
        }
    }

    fun getUser() : String {
        return this.users.random()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ChatChannel

        if (channelName != other.channelName) return false
        if (id != other.id) return false
        if (messages != other.messages) return false
        if (users != other.users) return false

        return true
    }

    override fun hashCode(): Int {
        var result = channelName.hashCode()
        result = 31 * result + id.hashCode()
        result = 31 * result + messages.hashCode()
        result = 31 * result + users.hashCode()
        return result
    }

    override fun toString(): String {
        return "ChatChannel(channelName='$channelName', id=$id, messages=$messages, users=$users)"
    }

}