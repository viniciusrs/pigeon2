package com.example.pigeon.ui.list.viewholders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.pigeon.R
import com.example.pigeon.model.ChatChannel
import com.example.pigeon.ui.list.adapter.ChatChannelAdapter

open class ChatChannelViewHolder(
    itemView: View,
    protected val adapter: ChatChannelAdapter
): ViewHolder(itemView) {
    private val txtChannelName =
        itemView.findViewById<TextView>(R.id.nomeCanal)
    private val txtLastMessage =
        itemView.findViewById<TextView>(R.id.ultimaMensagemCanal)
    protected lateinit var currentChannel: ChatChannel
    init {
        this.itemView.setOnClickListener {
            this.adapter
                .getOnClickTaskListener()?.onClick(this.currentChannel)
        }
    }
    open fun bind(channel: ChatChannel) {
        this.currentChannel = channel
        this.txtChannelName.text = this.currentChannel.channelName
        this.txtLastMessage.text = this.currentChannel.getLastMessage()
    }
}