package com.example.pigeon.ui.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.pigeon.R
import com.example.pigeon.model.ChatChannel
import com.example.pigeon.ui.list.viewholders.ChatChannelViewHolder

class ChatChannelAdapter(
    private var channels: ArrayList<ChatChannel>
): Adapter<ChatChannelViewHolder>() {

    fun interface OnClickChannelListener {
        fun onClick(channel: ChatChannel)
    }

    private var listener: OnClickChannelListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatChannelViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(
            R.layout.itemview_canal, parent, false
        )
        return ChatChannelViewHolder(itemView, this)
    }

    override fun onBindViewHolder(holder: ChatChannelViewHolder, position: Int) {
        val channel = this.channels[position]
        holder.bind(channel)
    }

    override fun getItemCount(): Int {
        return this.channels.size
    }

    fun setOnClickChannelListener(listener: OnClickChannelListener?) {
        this.listener = listener
    }

    fun getOnClickTaskListener(): OnClickChannelListener? {
        return this.listener
    }
}