package com.example.pigeon.ui.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.example.pigeon.R
import com.example.pigeon.model.ChatMessage
import com.example.pigeon.ui.list.viewholders.ChatMessageViewHolder

class ChatMessageAdapter(
    private var messages: ArrayList<ChatMessage>
): Adapter<ChatMessageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatMessageViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(
            R.layout.itemview_mensagem, parent, false
        )
        return ChatMessageViewHolder(itemView, this)
    }

    override fun onBindViewHolder(holder: ChatMessageViewHolder, position: Int) {
        val message = this.messages[position]
        holder.bind(message)
    }

    override fun getItemCount(): Int {
        return this.messages.size
    }
}