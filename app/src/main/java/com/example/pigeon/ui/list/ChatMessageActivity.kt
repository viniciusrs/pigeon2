package com.example.pigeon.ui.list

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pigeon.R
import com.example.pigeon.data.DAOChatChannelSingleton
import com.example.pigeon.model.ChatMessage
import com.example.pigeon.ui.list.adapter.ChatMessageAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.time.LocalDateTime

class ChatMessageActivity : AppCompatActivity() {
    private lateinit var rvMessageList: RecyclerView
    private lateinit var btnSendMessage: FloatingActionButton
    private lateinit var etxtMessage: EditText
    private var channelId: Long = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        this.rvMessageList = findViewById(R.id.recyclerView)
        this.btnSendMessage = findViewById(R.id.enviarMensagem)
        this.etxtMessage = findViewById(R.id.escreverMensagem)
        this.channelId = intent.getLongExtra("channelId", -1)
        val channel = DAOChatChannelSingleton.getChannelById(channelId)
        this.rvMessageList.layoutManager = LinearLayoutManager(this)
        if (channel != null) {
            this.rvMessageList.adapter = ChatMessageAdapter(channel.getMessages())
        }
    }

    override fun onBackPressed() {
        val output = Intent()
        output.putExtra("channelId", this.channelId)
        setResult(RESULT_OK, output)
        super.onBackPressed()
    }

    fun onClickInsertMessage(view: View) {
        val text = this.etxtMessage.text.toString()
        if(text.isNotBlank()) {
            val channel = DAOChatChannelSingleton.getChannelById(channelId)
            this.etxtMessage.text.clear()
            if (channel != null) {
                val author = channel.getUser()
                val m = ChatMessage(author, text, LocalDateTime.now())
                channel.addMessage(m)
                this.rvMessageList.adapter?.notifyItemInserted(channel.getMessages().size-1)
                this.rvMessageList.smoothScrollToPosition(channel.getMessages().size-1)
            }
        }
    }
}
