package com.example.pigeon.ui.list.viewholders

import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.pigeon.R
import com.example.pigeon.model.ChatMessage
import com.example.pigeon.ui.list.adapter.ChatMessageAdapter


open class ChatMessageViewHolder(
    itemView: View,
    protected val adapter: ChatMessageAdapter
): ViewHolder(itemView) {
    private val txtUser=
        itemView.findViewById<TextView>(R.id.nome)
    private val txtMessage =
        itemView.findViewById<TextView>(R.id.mensagem)
    private val txtTime =
        itemView.findViewById<TextView>(R.id.data)
    open fun bind(message: ChatMessage) {
        val dpRatio = itemView.context.resources.displayMetrics.density
        if (message.user!="Vinicius"){
            val constraint = itemView.findViewById<ConstraintLayout>(R.id.ConstrainLayout)
            constraint.background = ContextCompat.getDrawable(itemView.context, R.drawable.background_outras_mensagem)
            val params = constraint.layoutParams as RelativeLayout.LayoutParams
            params.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT)
            params.marginEnd = 50*dpRatio.toInt()
            params.marginStart = 8*dpRatio.toInt()
            constraint.layoutParams = params
        } else{
            val constraint = itemView.findViewById<ConstraintLayout>(R.id.ConstrainLayout)
            constraint.background = ContextCompat.getDrawable(itemView.context, R.drawable.background_minha_mensagem)
            val params = constraint.layoutParams as RelativeLayout.LayoutParams
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
            params.marginEnd = 8*dpRatio.toInt()
            params.marginStart = 50*dpRatio.toInt()
            constraint.layoutParams = params
        }
        this.txtUser.text = message.user
        this.txtMessage.text = message.message
        this.txtTime.text = message.formatDate()
    }
}
