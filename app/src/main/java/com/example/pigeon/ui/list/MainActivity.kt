package com.example.pigeon.ui.list

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pigeon.R
import com.example.pigeon.data.DAOChatChannelSingleton
import com.example.pigeon.ui.list.adapter.ChatChannelAdapter
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    private lateinit var rvChannelList: RecyclerView
    private lateinit var btnAddChannel: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_canais)
        this.rvChannelList = findViewById(R.id.rvTodoList)
        this.btnAddChannel = findViewById(R.id.adicionarCanal)
        this.rvChannelList.layoutManager = LinearLayoutManager(this)
        val adapter = ChatChannelAdapter(DAOChatChannelSingleton.getChannels())
        this.rvChannelList.adapter = adapter
        val resultLauncher = registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()
        ) { result ->
            if(result.resultCode == RESULT_OK && result.data != null) {
                val channelId = result.data!!.getLongExtra("channelId", -1)
                val channelPosition = DAOChatChannelSingleton.getChannelPositionById(channelId)
                adapter.notifyItemChanged(channelPosition)
                adapter.notifyDataSetChanged()
            }
        }
        adapter.setOnClickChannelListener { channel ->
            val openChannelIntent =
                Intent(this, ChatMessageActivity::class.java)
            openChannelIntent.putExtra("channelId", channel.id)
            resultLauncher.launch(openChannelIntent)
        }
    }

    fun onClickInsert(view: View) {
        DAOChatChannelSingleton.addChannel()
        this.rvChannelList.adapter?.notifyItemInserted(0)
        this.rvChannelList.smoothScrollToPosition(0)
    }
}
